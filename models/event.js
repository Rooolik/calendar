var connectDB = require('../libs/mongoose');
var crypto = require('crypto');
var async = require('async');
var util = require('util');
var userSchema = require('./user').user;

var mongoose = connectDB.mongoose;
var promise = connectDB.promise;

var Schema = mongoose.Schema;
var schema = new Schema({
    user_id: {
        type: String,
        required: true
    },
    fullDate: {
        type: String,
        required: true
    },
    events: [
        {
            title: String,
            from: String,
            to: String,
            description: String,
            time: String
        }
    ],
});

schema.statics.addEvent = function (title, from, to, description, time, user_id, collback) {

    var Event = this;

    async.waterfall([
            function (callback) {
                Event.findOne({fullDate: from, user_id: user_id}, callback);
            },
            function (event, callback) {
                if (event) {
                    Event.update({_id: event._id},
                        {
                            $push: {
                                events: {
                                    title: title,
                                    from: from,
                                    to: to,
                                    description: description,
                                    time: time
                                }
                            }
                        }, function(err, numAffected, rawResponse) {
      if (!err) Event.findOne({fullDate: from, user_id: user_id}, collback);
    });
                } else {
                    var newEvent = new Event({
                        user_id: user_id,
                        fullDate: from,
                        events: {
                            title: title,
                            from: from,
                            to: to,
                            description: description,
                            time: time
                        }
                    });
                    newEvent.save(callback)
                }
            }
        ],
        collback
    );
};

schema.statics.changeEvent = function (newEvent, from, user_id, outCallback) {
    var Day = this;

    async.waterfall([
        function (callback) {
         Day.findOne({fullDate: from, user_id: user_id}, callback)},
         function (day, callback) {
        day.events = day.events.map(function(event){
            if(event._id == newEvent._id){
                return newEvent
            }
            return event
        });
             day.save(callback)
         }
    ],
        outCallback)
    ;
};

module.exports.Event = mongoose.model('Event', schema);
