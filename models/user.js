var connectDB = require('../libs/mongoose');
var crypto = require('crypto');
var async = require('async');
var util = require('util');
var Event = require('./event').Event;

var mongoose = connectDB.mongoose;
var promise = connectDB.promise;


var Schema = mongoose.Schema;
var schema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    event: [{type: Schema.Types.ObjectId, ref: 'Event'}]
});

schema.methods.encryptPassword = function (password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function (password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () {
        return this._plainPassword;
    });

schema.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function (email, password, collback) {
    var User = this;
    async.waterfall([
            function (callback) {
                User.findOne({email: email}, callback);
            },
            function (user, callback) {
                if (user) {
                    if (user.checkPassword(password)) {
                        callback(null, user);
                    } else {
                        collback(new AuthError('Wrong password'))
                    }
                } else {
                    collback(new AuthError('We cannot find an account with that email'))
                }
            }
        ],
        collback
    )
};

schema.statics.signUp = function (email, firstname, lastname, password, callback) {
    var User = this;
    var user = new User({
        email: email,
        firstName: firstname,
        lastName: lastname,
        password: password
    });
    user.save(callback)

};

schema.statics.addEvent = function (title, from, to, description, callback) {

    var User = this;

    async.waterfall([
            function (insideCallback) {
                  Event.findOneAndUpdate({user_id: '59c8de4184a0ca072c07b026', fullDate: "2017-09-08"}, {
                        $push: {
                            events: {
                                title: "title2",
                                from: "2017-09-08",
                                to: "2017-09-08",
                                description: "desc2"
                            }
                        }
                    },insideCallback);
            },
            function (user, insideCallback) {
                console.log('user', user);
                if (!user) {
                    var newEvent = new Event({
                        fullDate: from,
                        event: {
                                title: "title2",
                                from: "2017-09-08",
                                to: "2017-09-08",
                                description: "desc2"
                            }
                    });
                    newEvent.save(insideCallback);

                } else {

                        insideCallback(null, user);
                }
            }

            // function (user, insideCallback) {
            //     if (user) {
            //         User.update({_id: '59c8de4184a0ca072c07b026'}, {
            //             $push: {
            //                 event: {
            //                     fullDate: "Sdfsdf",
            //                     events: {
            //                 title: title,
            //                 from: from,
            //                 to: to,
            //                 description: description
            //             }
            //                 }
            //             }
            //         });
            //     }else{
            //         callback(new AuthError('Wrong password'))
            //     }

            // if (user.event.fullDate) {
            //     event.update({_id: event._id},
            //         {
            //             $push: {
            //                 event: {
            //                     title: title,
            //                     from: from,
            //                     to: to,
            //                     description: description
            //                 }
            //             }
            //         }, callback);
            // } else {
            //     var newEvent = new User({
            //         fullDate: from,
            //         event: {
            //             title: title,
            //             from: from,
            //             to: to,
            //             description: description
            //         }
            //     });
            //     newEvent.save(callback)
            // }
            // }
        ],
        callback
    );
};

module.exports.User = mongoose.model('User', schema);

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);
    this.message = message;
}

util.inherits(AuthError, Error);
AuthError.prototype.name = 'AuthError';
exports.AuthError = AuthError;
