
export default function user(state = {}, action) {
    if (action.type == "LOGIN") {
        return action.data
    }

    if (action.type == "LOG_OUT") {
        return {}
    }

    return state
}

