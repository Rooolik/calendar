import {combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import user from './user'
import error from './error'
import calendarData from './calendarData'

export default combineReducers({
routing: routerReducer, user, error, calendarData
})
