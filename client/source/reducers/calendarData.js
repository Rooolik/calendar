import moment from 'moment';

function getDay() { // получить номер дня недели, от 0(пн) до 6(вс)
    let firstDay = moment().date(1).day();
    let today = moment();
    if (firstDay == 1) {
        firstDay = moment().date(1)
    } else {
        firstDay = moment().day(-7);
        firstDay = firstDay.date(firstDay.date() + 1);
    }
    return firstDay.format("YYYY-MM-DD")

}

export default function calendarData(state = {
    startFromDate: getDay(), events: {}, selectedDay: {}, today: moment().format("YYYY-MM-DD"),
}, action) {

    if (action.type == "LOG_OUT") {
        return {
            startFromDate: getDay(), events: {}, selectedDay: {}
        }
    }


    if (action.type == "PLUS_WEEK") {
        let newDate = moment(state.startFromDate);
        let tt = newDate.date() + 7;
        return Object.assign({}, state, {startFromDate: newDate.date(tt)});
    }

    if (action.type == "MINUS_WEEK") {
        let newDate = moment(state.startFromDate);
        let tt = newDate.date() - 7;
        return Object.assign({}, state, {startFromDate: newDate.date(tt)});
    }

    if (action.type == "SET_TODAY") {
        return Object.assign({}, state, {startFromDate: getDay()});
    }

    if (action.type == "SET_INIT_EVENTS") {
        let events = action.data;
        let newEvents = {};
        events.forEach((item, i, arr) => {
            newEvents[item.fullDate] = item.events
        });
        return Object.assign({}, state, {events: newEvents});
    }

    if (action.type == "ADD_EVENT") {
        const newEvents = Object.assign({}, state.events, {[action.data.from]: action.data.event});

        return Object.assign({}, state, {events: newEvents});

    }

    if (action.type == "CHANGE_EVENT") {
        const newEvents = Object.assign({}, state.events, {[action.data.from]: action.data.event});
        return Object.assign({}, state, {events: newEvents});
    }

    if (action.type == "SELECT_DAY") {
        return Object.assign({}, state, {selectedDay: action.day});
    }

    return state
}

