import React, {Component} from 'react';
import {connect} from 'react-redux'

import './MoreDay.less'


class MoreDay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedEvent: "",
            selectedForm: "",
            title: ""
        };

    }

    setValidationError(field){
        this.setState({validationError: field})
    }

    getValue() {
        let newEvent;
        if (this.state._id) {
            if (this.state.selectedEvent.title != this.state.title || this.state.selectedEvent.from != this.props.selectedDay.id
                || this.state.selectedEvent.to != this.state.to || this.state.selectedEvent.description != this.state.description) {
                newEvent = {
                    title: this.state.title,
                    from: this.props.selectedDay.id,
                    to: this.state.to,
                    description: this.state.description,
                    _id: this.state._id,
                    time: this.state.time
                };
            }
        }
        return newEvent

    }

    _selectEvent(i) {
        this.setState({
            selectedForm: "",
            selectedEvent: i,
            title: i.title,
            from: this.props.selectedDay.id,
            to: i.to,
            description: i.description,
            _id: i._id,
            time: i.time
        })

    }

    _selectForm(form) {
        this.setState({selectedForm: form});
    }

    _changeTitle(e) {
        this.setState({title: e.target.value});
    }
    _changeDescription(e){
        this.setState({description: e.target.value});
    }

    _renderTitle() {
        let title = this.state.title;
        let error = this.state.validationError == "title"? "MoreDay-invalid-field" : null;
        if (this.state.selectedForm == "title") {
            title = <input
                className={`MoreDay-changed-form ${error}`}
                defaultValue={title}/>
        }
        return <div className="MoreDay-info-title"
                    onClick={this._selectForm.bind(this, "title")}
                    onChange={this._changeTitle.bind(this)}>
            {title}
        </div>
    }

    _renderDescription(){
         let error = this.state.validationError == "description" ? "MoreDay-invalid-field" : null;
        let description = this.state.description;
        if (this.state.selectedForm == "description") {
            description = <textarea
                className={`MoreDay-changed-form ${error}`}
                rows="3"
                defaultValue={description}/>
        }
        return <div
            className={`MoreDay-info-description ${error}`}
            onClick={this._selectForm.bind(this, "description")}
            onChange={this._changeDescription.bind(this)}>
            {description}
            </div>
    }

    _renderEvents() {
        if (this.props.events[this.props.selectedDay.id]) {
            return this.props.events[this.props.selectedDay.id].map((elem, i) => {
                let time = elem.time[0] + elem.time[1] + " : " + elem.time[2] + elem.time[3];
                let title;
                if (elem.title) {
                    title = elem.title.length > 12 ? elem.title.substr(0, 12) + "..." : elem.title;
                }


                return <div className="MoreDay-day-list"
                            key={i}>
                    <div className="MoreDay-time clearfix">
                        <span> {time}</span>
                    </div>
                    <div className="MoreDay-event-title-wrap"
                         onClick={this._selectEvent.bind(this, elem)}>
                        <div className="MoreDay-event-title">
                            {title}
                        </div>
                    </div>
                </div>
            });
        } else {
            return <h1 className="MoreDay-info-title">No events</h1>
        }
    }

    _renderInfo() {
        let fromTo;
        if (this.state.to) {
            fromTo = <div>
                <div className="MoreDay-info-description">
                    from: <span>{this.state.from}</span>
                </div>
                <div className="MoreDay-info-description">
                    to: <span>{this.state.to}</span>
                </div>
            </div>
        } else {
            fromTo = <div className="MoreDay-info-description">
                <span>{this.state.from}</span>
            </div>
        }

        return <div className="MoreDay-info">
            <div className="MoreDay-info-title">
                {this._renderTitle()}
            </div>
            <hr></hr>
            {fromTo}
            <hr></hr>
            {this._renderDescription()}

        </div>
    }

    render() {
        return <div className="MoreDay-container">
            <div className="MoreDay-column">
                {this._renderEvents()}
            </div>
            <div className="MoreDay-column">
                {this.state.selectedEvent ? this._renderInfo() : null}
            </div>
        </div>


    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        selectedDay: state.calendarData.selectedDay,
        events: state.calendarData.events,
    }
};

export default connect(mapStateToProps, null, null, {withRef: true})(MoreDay)
