import React, {Component} from "react";
import "./App.less";
import {connect} from "react-redux";
import axios from "axios";
import Menu from "./Menu.jsx";


class App extends Component {

    componentDidMount() {
        let _this = this;
        axios({
            method: 'get',
            url: '/init',
            headers: {"X-Requested-With": "XMLHttpRequest"}
        }).then(function (response) {
            let user = response.data.user;
            _this.props.onInitUser(user.id, user.firstName, user.lastName, user.email);
            _this.props.onInitialEvents(response.data.events)
        }).catch(function (error) {
            console.log("err", error);
        });
    }

    render() {
        return (
            <div>
                <Menu/>
                {this.props.children}
            </div>

        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        ownProps,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onInitUser: (id, firstName, lastName, email) => {
            dispatch({
                type: "LOGIN",
                data: {id: id, firstName: firstName, lastName: lastName, email: email}
            })
        },
        onInitialEvents: (events) => {
             dispatch({
                type: "SET_INIT_EVENTS",
                data:  events,
            })
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App)

