import React, {PropTypes} from 'react';
import {Link} from 'react-router'

import './Button.less'

const Button = (props) => {
    let size = '';
    if(props.size){
        size = props.size + '-button';
        if(props.href){
            size = props.size + '-button-a'
        }
    }
    switch (props.color) {
        case 'blue':
            return (
                props.href
                    ? <Link to={props.href} className={`button blue-button ${size}`} {...props}/>
                    : <button className={`button blue-button ${size}`} {...props}/>
        );
        case 'white':
            return (
                props.href
                    ? <Link to={props.href} className={`button white-button ${size}`} {...props}/>
                    : <button className={`button white-button ${size}`}  {...props}/>
        );
        case 'blue-submit':
            return <button type="submit" className={`button white-button ${size}`}  {...props}/>

    }
};


Button.propTypes = {
    href: PropTypes.string,
};

export default Button
