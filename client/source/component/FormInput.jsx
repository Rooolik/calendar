import React, {Component} from 'react';
import './FormInput.less'
import 'react-date-picker/index.css'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: moment()
        };
        this.handleChange = this.handleChange.bind(this);

    }

    getValue() {
        return 'value' in this.refs.input
            ? this.refs.input.value
            : this.refs.input.getValue();
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    render() {
        const common = {
            ref: 'input',
        };
        let date = '2017-04-24';
        const onChange = (dateString, {dateMoment, timestamp}) => {
        };

        switch (this.props.type) {
            case "password":
                return <input className="Input" {...common} defaultValue={this.props.default} placeholder={this.props.placeholder} type="password"/>;
            case "text-area":
                return <textarea className="textArea" placeholder={this.props.placeholder}{...common} />;
            case "dialog-date-input":
                return <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleChange}/>;
            default:
                return <input className="Input" {...common} placeholder={this.props.placeholder} defaultValue={this.props.default} type="text"/>;
        }
    }
}

export default Input
