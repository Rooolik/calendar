import React, {Component} from "react";
import FormInput from "./FormInput.jsx";
import Button from "./Button.jsx";
import "./LoginPage.less";
import {connect} from "react-redux";
import createUser from "../actions/createUser.js";
import checkEmail from "../utils/checkEmail";
import checkName from "../utils/checkName";


class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            firstName: "",
            lastName: "",
            error: [],
        }
    }

    signup(e) {
        let error = [];
        if (this.props.error) error = [this.props.error];
        if (!checkEmail(this.state.email)) {
            error.push("The email address you entered is not valid");
        }
        if (!checkName(this.state.firstName)) {
            error.push("The first name you entered is not valid");
        }
        if (!checkName(this.state.lastName)) {
            error.push("The last name you entered is not valid");
        }
        if (error[0]) {
            this.setState({error: error})
        } else {
            this.props.onCreateUser(this.state.email, this.state.password, this.state.firstName, this.state.lastName);
        }

    }


    emailChange(e) {
        this.setState({email: e.target.value,});
    }

    passwordChange(e) {
        this.setState({password: e.target.value,});
    }

    firstNameChange(e) {
        this.setState({firstName: e.target.value,});
    }

    lastNameChange(e) {
        this.setState({lastName: e.target.value,});
    }

    render() {

        return <div className="loginPage-container">
                <div className="loginPage-row"><h1>Sign Up</h1></div>
                <div onChange={this.emailChange.bind(this)} className="loginPage-row"><FormInput
                    placeholder="E-mail"/></div>
                <div onChange={this.firstNameChange.bind(this)} className="loginPage-row padding"><FormInput
                    placeholder="First Name"/></div>
                <div onChange={this.lastNameChange.bind(this)} className="loginPage-row padding"><FormInput
                    placeholder="Last Name"/></div>
                <div onChange={this.passwordChange.bind(this)} className="loginPage-row padding"><FormInput
                    placeholder="Password" type="password"/></div>
                <ul className="error">
                    { this.state.error.map(function (error, idx) {
                         return error ? <li key={idx}>{error}</li> : null
                    })
                    }
                </ul>
                <div className="loginPage-row">
                    <div className="padding">
                        <Button onClick={this.signup.bind(this)} color="blue" size="big">Sign Up</Button>
                    </div>
                    <Button href="/login" color="white" size="big">Log In</Button>
                </div>

            </div>

    }

}

const mapStateToProps = (state, ownProps) => {


    return {
        user: state.user,
        error: state.error.errorMessage,
        ownProps,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: (email, password, firstName, lastName) => {
            dispatch(createUser(email, password, firstName, lastName));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
