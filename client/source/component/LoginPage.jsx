import React, {Component} from "react";
import FormInput from "./FormInput.jsx";
import Button from "./Button.jsx";
import "./LoginPage.less";
import {connect} from "react-redux";
import getUser from "../actions/getUser.js";
import checkEmail from "../utils/checkEmail";


class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: [this.props.error],
        };
    }

    login(e) {
        e.preventDefault();
        if (!checkEmail(this.email.getValue())){
            let error = [this.props.error];
            error.push("The email address you entered is not valid");
            this.setState({error: error});
        }else{
            this.props.onGetUser(this.email.getValue(), this.password.getValue());
        }
    }

    render() {
        return (<form onSubmit={this.login.bind(this)}><div className="loginPage-container">
            <div className="loginPage-row"><h1>Log in</h1></div>
            <div className="loginPage-row">
                <FormInput
                placeholder="E-mail"
                default="test@mail.ru"
                ref={(input) => { this.email = input; }}/>
            </div>
            <div className="loginPage-row padding">
                <FormInput
                placeholder="Password"
                type="password"
                default="test"
                ref={(input) => { this.password = input; }}/>
            </div>
            <ul className="error">
                { this.state.error.map(function (error, idx) {
                        return error ? <li key={idx}>{error}</li> : null
                    })
                }
                </ul>
            <div className="loginPage-row">
                <div className="padding">
                    <Button type="submit" color="blue" size="big">Log IN</Button>
                </div>
                <Button href="/signup" color="white" size="big">Sign Up</Button>
            </div>

        </div></form>)

    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        error: state.error.errorMessage,
        ownProps,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onGetUser: (email, password) => {
            dispatch(getUser(email, password));
        },
    }

};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
