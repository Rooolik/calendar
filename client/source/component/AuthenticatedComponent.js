import React from 'react';
import {connect} from 'react-redux';

export function requireAuthentication(Component, RedirectComponent) {

    class AuthenticatedComponent extends React.Component {

        componentWillMount() {
            this.checkAuth(this.props.isAuthenticated);

        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps.isAuthenticated);
        }

        checkAuth(isAuthenticated) {
            if (!isAuthenticated) {
                let redirectAfterLogin = this.props.location.pathname;

            }
        }

        render() {
            return (
                <div>
                    {this.props.isAuthenticated === true
                        ? <Component/>
                        : <RedirectComponent/>
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        user: state.user,
        isAuthenticated: !!state.user.id
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}
