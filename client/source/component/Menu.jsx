import React, {Component} from "react";
import {connect} from "react-redux";
import "./Menu.less";
import Button from "./Button.jsx";
import logOut from "../actions/logOut";

class Menu extends Component {

    _renderMenu() {

        let welcome = !!this.props.user.firstName ? "welcome " + this.props.user.firstName : null;
        let buttonLogin;
        let buttonSignUp;

        if (this.props.user.id) {
            buttonLogin = <div className="menu-right"><Button color="blue" onClick={() => {
                this.props.onLogOut()
            }}>Log Out</Button></div>;
        }
        else {

            buttonLogin = <div className="menu-right"><Button color="blue" href="/login">Log In</Button></div>;
            buttonSignUp = <div className="menu-right"><Button color="blue" href="/signup">Sign Up</Button></div>;

        }
        return <div className="Menu clearfix">
            <span className="menu-left">{welcome}</span>
            {buttonLogin}{buttonSignUp}
        </div>
    }

    render() {
        return this._renderMenu()
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onLogOut: () => {
            dispatch(logOut());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
