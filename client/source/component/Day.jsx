import React, {Component} from 'react';
import {connect} from 'react-redux'

import './Day.less'
import Button from './Button.jsx'

class Day extends Component {


    getValue() {
        this.props.onDoubleClick();
        this.props.onSelectDay(this.props.date);
    }

    actionOnClick() {
        this.props.onSelectDay(this.props.date);
        this.props.actionOnClick();

    }

    renderDay() {
        let today = this.props.today == this.props.date.id ? "today" : "";
        let events = this.props.events[this.props.date.id];
        let context;
        let weekendClass = this.props.isWeekend ? "weekend" : "";
        if (events) {
            context = ( events.map((elem, i) => {
                    let eventsClass = "events s" + i;
                    let title;
                    if (elem.title) {
                        title = elem.title.length > 12 ? elem.title.substr(0, 12) + "..." : elem.title;
                    }

                    if (i < 3) {
                        return (
                            <div key={i} className={eventsClass}>
                                <Button
                                    size="event"
                                    color="blue">
                                    {title}
                                </Button>
                            </div>)
                    } else if (i == 3) {
                        return (
                            <div key={i}
                                 className={eventsClass}>
                                <Button
                                    size="event"
                                    color="blue">
                                    More...
                                </Button>
                            </div>)
                    }
                })
            )
        }

        return (<div className={`day ${weekendClass}`}
                     onDoubleClick={this.actionOnClick.bind(this)}>
                    <div className={`day-date ${today}`}>
                        {this.props.date.day}
                    </div>
                    <div className="MoreDay-plus-button">
                        <Button
                            size="small-plus"
                            color="blue"
                            onClick={this.getValue.bind(this)}>
                            +
                        </Button>
                    </div>
                    {context}
                </div>)
    }

    render() {
        return this.renderDay()
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        events: state.calendarData.events,
        selectedDay: state.calendarData.selectedDay,
        today: state.calendarData.today,
        ownProps,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSelectDay: (day) => {
            dispatch({type: "SELECT_DAY", day: day});
        },

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Day)
