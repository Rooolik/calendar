import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

import './FormNewEvent.less'
import Suggest from './Suggest.jsx'


class FormNewEvent extends Component {

    constructor(props) {
        super(props);
        let date = new Date();
        let minutes = date.getMinutes();
        minutes = minutes < 10 ? "0"+minutes : minutes;
        this.state = {
            fromDate: moment([this.props.selectedDay.year, this.props.selectedDay.month, this.props.selectedDay.day]).hour(4),
            toDate: moment([this.props.selectedDay.year, this.props.selectedDay.month, this.props.selectedDay.day]),
            hours: date.getHours(),
            minutes: minutes,
            error: '',
        };
        this.fromHandleChange = this.fromHandleChange.bind(this);
        this.toHandleChange = this.toHandleChange.bind(this);
        this.inputChange = this.inputChange.bind(this);
        this.descriptionChange = this.descriptionChange.bind(this);
        this.onChangeHours =  this.onChangeHours.bind(this);
         this.onChangeMinutes = this.onChangeMinutes.bind(this);
    }

    getValue() {
        return {
            fromDate: this.state.fromDate,
            toDate: this.state.toDate,
            title: this.state.title,
            description: this.state.description,
            time: this.state.hours+""+this.state.minutes,
        }
    }
    setError() {
        this.setState({error: "invalid-title"})
    }

    inputChange(e) {
        this.setState({title: e.target.value})
    }

    descriptionChange(e) {
        this.setState({description: e.target.value})
    }

    fromHandleChange(date) {
        this.setState({
            fromDate: date
        });
    }

    toHandleChange(date) {
        this.setState({
            toDate: date
        });
    }

    onChangeHours(e){
        let hours = +e.target.value;
        if(hours){
        if(+hours < 25) {
           if(+hours < 0){
               hours = "00"
           }else if(+hours < 10){
                hours = "0"+hours;
           }
        }else{
            hours = "00"
        }
        }else {
            hours = "00"
        }
        this.setState({hours: hours})
    }
    onChangeMinutes(e){
        let minutes = +e.target.value;
        if(minutes){
        if(+minutes/60 < 1) {
                if(minutes<10){
                    minutes = "0"+minutes;
                }
            }else{
                minutes = "00";
            }
        }else {
            minutes = "00";
        }
        this.setState({minutes: minutes})
    }

    render() {
        let date = new Date();
        return <div className="dialog-add-event">
            <div className={`dialog-row`}>
                <label htmlFor="title">Title</label>
                <input className={`input-dialog ${this.state.error}`}
                       id="title"
                       onChange={this.inputChange}/>
            </div>
            <div className="dialog-row">
                <div className="from-to">
                    <label htmlFor="from">From</label>
                    <DatePicker
                        id="from"
                        selected={this.state.fromDate}
                        onChange={this.fromHandleChange}
                        />
                </div>
                <div className="from-to to-margin">
                    <label htmlFor="to">To</label>
                    <DatePicker
                        id="to"
                        selected={this.state.toDate}
                        onChange={this.toHandleChange}/>
                </div>
                <div className="from-to to-margin">
                    <label htmlFor="to">Time</label>
                    <input className="small-input"
                           value={this.state.hours}
                           onChange={this.onChangeHours}/>
                </div>
                <span className="small-padding">:</span>
                <div className="from-to">
                    <input className="small-input"
                           value={this.state.minutes}
                           onChange={this.onChangeMinutes}/>
                </div>
            </div>
            <div className="dialog-row">
                <label htmlFor="description">Description</label>
                <textarea rows="4" id="description" onChange={this.descriptionChange} className="input-dialog"/>
            </div>

        </div>
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        selectedDay: state.calendarData.selectedDay,
        ownProps,
    }
};

export default connect(mapStateToProps, null, null, {withRef: true})(FormNewEvent);
