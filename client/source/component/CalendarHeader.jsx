import React, {Component} from 'react';
import './App.less'
import {connect} from 'react-redux'
import moment from 'moment';

import './Calendar.less'
import './CalendarHeader.less'
import Button from './Button.jsx'


class CalendarHeader extends Component {

    render() {

        return <div className="head-menu-container">
            <div className="head-menu-wrap">
                <div className="header-date">
                    {moment(this.props.startFromDate).add(14, 'days').format("MMMM YYYY")}
                </div>
                <div className="button-set">
                    <Button color="blue" onClick={() => {
                        this.props.onMinusWeek()
                    }}>&lt;Previous</Button>
                    <Button color="blue" onClick={() => {
                        this.props.onToday()
                    }}>Today</Button>
                    <Button color="blue" onClick={() => {
                        this.props.onPlusWeek()
                    }}>Next&gt;</Button>
                </div>

            </div>

            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Monday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Tuesday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Wednesday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Thursday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Friday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Saturday</div>
            </div>
            <div className="head-menu-day-wrap">
                <div className="head-menu-day">Sunday</div>
            </div>
        </div>
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        startFromDate: state.calendarData.startFromDate,
        ownProps,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPlusWeek: () => {
            dispatch({type: "PLUS_WEEK"});

        },
        onMinusWeek: () => {
            dispatch({type: "MINUS_WEEK"});

        },
        onToday: () => {
            dispatch({type: "SET_TODAY"});

        },

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarHeader)

