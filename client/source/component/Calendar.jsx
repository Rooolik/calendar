import React, {Component} from 'react';
import './App.less'
import {connect} from 'react-redux'
import moment from 'moment';
import './Calendar.less'
import Day from './Day.jsx'
import CalendarHeader from './CalendarHeader.jsx'
import Dialog from './Dialog.jsx'
import MoreDay from './MoreDay.jsx'
import FormNewEvent from './FormNewEvent.jsx'
import addEvent from '../actions/addEvent'
import getEvent from '../actions/getEvent'
import checkName from '../utils/checkName'
import checkDescription from '../utils/checkDescription'
import changeEvent from '../actions/changeEvent'


class Calendar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addNew: false,
            allEvent: false
        };

    }

    componentDidMount() {
        if (this.props.user.id) {
            this.props.onGetEvents();
        }
    }

    //please, rename this method
    createPeriod(date) {
        let day = moment(date);
        let period = [];
        let week = [];
        while (period.length < 5) {
            week.push({
                id: day.format("YYYY-MM-DD"),
                year: day.year(),
                month: day.month(),
                day: day.date()
            });
            if (week.length == 7) {
                period.push(week);
                week = [];

            }
            day.date(day.date() + 1)
        }
        return period;
    }

    _renderNewEventDialog() {
        return (this.state.addNew ?
                <Dialog
                    modal={this.state.addNew}
                    header="Add new event"
                    onAction={this._addNew.bind(this)}>
                    <FormNewEvent ref="dialogInput"/>
                </Dialog> : null
        )
    }

    _renderAllEventDialog() {
        return (this.state.allEvent ?
                <Dialog
                    modal={this.state.allEvent}
                    header={moment(this.props.selectedDay.id).format("D MMMM YYYY")}
                    onAction={this._removeAllEvenDialog.bind(this)}
                    hasCancel={false}>
                    <MoreDay ref="dialogAllEvent"/>
                </Dialog> : null
        )
    }


    _addNewDialog() {
        this.setState({
            addNew: true,
        });
    }

    _addAllEventDialog() {
        this.setState({
            allEvent: true,
        });
    }

    _closeAllEventDialog(){
        this.setState({
                allEvent: false,
            });
    }

    _removeAllEvenDialog() {
        let data = this.refs.dialogAllEvent.getWrappedInstance().getValue();
        if(!data){
           this._closeAllEventDialog()
        }else if (!checkName(data.title) || !checkDescription(data.description) ) {
            let error = checkName(data.title)? "description" : "title";
            this.refs.dialogAllEvent.getWrappedInstance().setValidationError(error);
        }else {
            if (data) {
                this.props.onChangeEvent(data, this.props.selectedDay.id);
            }
            this._closeAllEventDialog()
        }
    }

    _addNew(action) {
        if (action === 'dismiss') {
            this.setState({addNew: false});
        }
        let data = this.refs.dialogInput.getWrappedInstance().getValue();
        if (!checkName(data.title)) {
            this.refs.dialogInput.getWrappedInstance().setError();
        } else if (action === 'confirm') {
            this.setState({addNew: false});
            this.props.onAddEvent(data.title, data.fromDate.format("YYYY-MM-DD"), data.toDate.format("YYYY-MM-DD"), data.description, data.time)
        }
    }

    _renderCalendar() {
        let period = this.createPeriod(this.props.firstDay);
        return period.map(function (item, i, arr) {
            return (
                <div className="calendar-row"
                     key={i}>
                    {item.map((day, i) => {
                        return <Day date={day}
                                    onDoubleClick={this._addNewDialog.bind(this)}
                                    actionOnClick={this._addAllEventDialog.bind(this)}
                                    key={i + i}
                                    ref="day"
                                    isWeekend={i > 4}/>
                    }, this)}
                </div>)
        }, this);
    }

    render() {
        return <div className="calendar-table">
            <CalendarHeader/>
            {this._renderCalendar()}
            {this._renderNewEventDialog()}
            {this._renderAllEventDialog()}
        </div>
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        firstDay: state.calendarData.startFromDate,
        event: state.calendarData.events,
        selectedDay: state.calendarData.selectedDay,
        ownProps
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onAddEvent: (title, from, to, description, time) => {
            dispatch(addEvent(title, from, to, description, time));
        },
        onGetEvents: () => {
            dispatch(getEvent())
        },
        onChangeEvent: (event, from) => {
            dispatch(changeEvent(event, from))
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar)

