const getInitData = (email, password) => {
    return (dispatch) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/init', true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send();
        let res;
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;
            if (xhr.status == 200) {
                try {
                    res = JSON.parse(xhr.responseText);
                }
                catch (err) {
                    if (err.message = "SyntaxError") {
                        res = {};
                    }
                }

                dispatch({
                    type: "LOGIN",
                    data: {id: res.id, firstName: res.firstName, lastName: res.lastName, email: res.email}
                })
            }

        };
    }
};

export default getInitData
