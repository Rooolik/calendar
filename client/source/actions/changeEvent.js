import axios from "axios";

const changeEvent = (event, from) => {
    return (dispatch) => {


        axios({
            method: 'put',
            url: '/events',
            headers: {"X-Requested-With": "XMLHttpRequest"},
            data: {
                event: event,
                from: from
            }
        }).then(function (res) {
            dispatch({
                type: "CHANGE_EVENT",
                data: {event: res.data.event, from: res.data.from}
            })
        }).catch(function (error) {
            console.log("err", error);
        });
    }
};

export default changeEvent

