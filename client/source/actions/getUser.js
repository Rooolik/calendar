const getUser = (email, password) => {
    return (dispatch) => {
        let body = JSON.stringify({
            email: email,
            password: password,
        });
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/login', true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send(body);
        let res;
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;
            if (xhr.status == 200) {
                try {
                    res = JSON.parse(xhr.responseText);
                }
                catch (err) {
                    if (err.message = "SyntaxError") {
                        res = {};
                    }
                }
                dispatch({
                    type: "LOGIN",
                    data: {id: res.id, firstName: res.firstName, lastName: res.lastName, email: res.email}
                })
            }else{
                res = JSON.parse(xhr.responseText);
                dispatch({
                    type: "ERROR_LOGIN",
                    data: {errorMessage: res.message, errorStatus: res.status}
                })
            }

        };
    }
};

export default getUser
