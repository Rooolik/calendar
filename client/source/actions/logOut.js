
const logOut = (email, password) => {
    return (dispatch) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/logout', true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send();
        console.log("logout", xhr);
        dispatch({type: "LOG_OUT", data: {}});
    }
};

export default logOut
