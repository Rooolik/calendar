const addEvent = (title, from, to, description, time) => {
    return (dispatch) => {
        let body = JSON.stringify({
            title: title,
            from: from,
            to: to,
            description: description,
            time: time
        });
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/add-event', true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send(body);
        let res;
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;
            if (xhr.status == 201) {
                try {
                    res = JSON.parse(xhr.responseText);
                }
                catch (err) {
                    if (err.message = "SyntaxError") {
                        res = {};
                    }
                }
                dispatch({
                    type: "ADD_EVENT",
                    data: {event: res.event, from: res.from}
                })
            }else{
                res = JSON.parse(xhr.responseText);
                dispatch({
                    type: "ERROR_LOGIN",
                    data: {errorMessage: res.message, errorStatus: res.status}
                })
            }

        };
    }
};

export default addEvent

