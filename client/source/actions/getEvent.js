import axios from 'axios';

const addEvent = () => {
    return (dispatch) => {
        axios({
            method: 'get',
            url: '/events',
            headers: {"X-Requested-With": "XMLHttpRequest"}
        }).then(function (response) {
            dispatch({
                type: "SET_INIT_EVENTS",
                data: response.data
            })
        }).catch(function (error) {
            console.log("error", error);
        });
    }
};

export default addEvent
