export default function checkDescription(name) {
    if(!name){
        return true
    }
    let result = name.match(/^[_a-zA-Z0-9а-яА-Я !@#$%^&():;".]+$/);
    return (result && result[0] == name)
}
