export default function checkEmail(name) {
   if(!name) {
      return false
   }
    let result = name.match(/^[_a-zA-Z0-9а-яА-Я ]+$/);
    return (result && result[0] == name)
}
