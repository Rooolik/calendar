'use strict';

import React from 'react'
import ReactDOM from 'react-dom'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import {composeWithDevTools} from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk'
import {syncHistoryWithStore} from 'react-router-redux'
import reducer from './source/reducers'
import {Router, Route, browserHistory} from 'react-router'

import LoginPage from './source/component/LoginPage.jsx'
import Calendar from './source/component/Calendar.jsx'
import SignUp from './source/component/SignUp.jsx'
import {requireAuthentication} from './source/component/AuthenticatedComponent'

import App from './source/component/App.jsx'

const store = createStore(reducer, composeWithDevTools(applyMiddleware(ReduxThunk)));
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route component={App}>
                <Route exact path="/" component={requireAuthentication(Calendar, LoginPage)}/>
                <Route path="login" component={requireAuthentication(Calendar, LoginPage)}/>
                <Route path="signup" component={requireAuthentication(Calendar, SignUp)}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('main')
);
