module.exports = function (req, res, next) {
    res.sendHttpError = function (error, next) {
        res.status(error.status);
        if (res.req.headers['x-requested-with'] == 'XMLHttpRequest') {
            res.json(error);
        } else {
            res.send({error: error})
        }
    };
    next();
};
