var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
var promise = mongoose.connect('mongodb://CalendarAdmin:123qweasd@ds040637.mlab.com:40637/calendar', {
    useMongoClient: true,
    config: {autoIndex: false}
});

module.exports.mongoose = mongoose;
module.exports.promise = promise;
