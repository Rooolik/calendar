var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var router = require('./routes/index');
var bodyParser = require('body-parser');
var HttpError = require('./error/index').HttpError;
var config = require('./config');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var mongoose = require('./libs/mongoose').mongoose;

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: config.get('session:secret'),
    key: config.get('session:key'),
    cookie: config.get('session:cookie'),
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(require('./middleware/sendHttpError'));
app.use(require('./middleware/loadUser'));
app.use(express.static(path.join(__dirname, 'client')));
app.use('/', router);



app.use(function (err, req, res, next) {
    if (typeof err == 'number') {
        err = new HttpError;
    }
    if (err instanceof HttpError) {
        res.sendHttpError(err);
    } else if (app.get('env') == 'development') {
        return next(err)
    }
    else {
        err = new HttpError(500);
        res.sendHttpError(err);
    }

});

module.exports = app;

