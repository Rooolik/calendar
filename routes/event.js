var Event = require('../models/event').Event;
var User = require('../models/user').User;
var AuthError = require('../models/user').AuthError;
var HttpError = require('../error/index').HttpError;
var async = require('async');

function compareNumeric(a, b) {
    if (+a.time > +b.time) return 1;
    if (+a.time < +b.time) return -1;
}

module.exports.post = function (req, res, next) {
    var title = req.body.title;
    var from = req.body.from;
    var to = req.body.to;
    var description = req.body.description;
    var time = req.body.time;
    var user_id = req.session.user;

    Event.addEvent(title, from, to, description, time, user_id, function (err, event) {
        if (err) {

            return next(err);
        }

        var newEvent = event.events.sort(compareNumeric);
        res.status(201).json({
            from: event.fullDate,
            event: newEvent
        });
    });
};

module.exports.get = function (req, res, next) {
    Event.find({user_id: req.session.user}, function (err, events) {
        if (!err) {
            res.status(200).json(events.sort(compareNumeric));
        }
    });
};

module.exports.put = function (req, res, next) {
    var event = req.body.event;
    var from = req.body.from;
    var user_id = req.session.user;
    Event.changeEvent(event, from, user_id, function (err, day) {
        if (err) {
            return next(err);
        }
        var updatedEvents = day.events.sort(compareNumeric);
        res.status(200).json({
            from: day.fullDate,
            event: updatedEvents
        });
    });
};
