var User = require('../models/user').User;
var AuthError = require('../models/user').AuthError;
var HttpError = require('../error/index').HttpError;
var async = require('async');
var path = require('path');



module.exports.loginPost = function (req, res, next) {

    var email = req.body.email;
    var password = req.body.password;

    User.authorize(email, password, function (err, user) {
        if (err) {
            if (err instanceof AuthError) {
                return next(new HttpError(403, err.message));
            }
            else {
                return next(err);
            }
        }

        req.session.user = user._id;
        res.json({
             id: user._id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
        });
    });
};

