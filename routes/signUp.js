var User = require('../models/user').User;
var AuthError = require('../models/user').AuthError;
var HttpError = require('../error/index').HttpError;
var async = require('async');

module.exports.post = function (req, res, next) {

    var email = req.body.email;
    var password = req.body.password;
    var firstname = req.body.firstName;
    var lastname = req.body.lastName;


    User.signUp(email, firstname, lastname, password, function (err, user) {
        if (err) {
            return next(err);
        }
        req.session.user = user._id;
        res.status(201).json({
            id: user._id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
        });
    });
};

