var User = require('../models/user').User;
var AuthError = require('../models/user').AuthError;
var HttpError = require('../error/index').HttpError;
var async = require('async');
var path = require('path');
var Event = require('../models/event').Event;

   function compareNumeric(a, b) {
  if (+a.time > +b.time) return 1;
  if (+a.time < +b.time) return -1;
}


module.exports = function (req, res, next) {
    console.log("searchUser");
    if (!req.session.user) {
        var error = new HttpError(401, 'Unauthorized');
        if (res.req.headers['x-requested-with'] == 'XMLHttpRequest') {
            res.status(401).json(error);
        } else {
            res.send({error: error})
        }
    } else {
        User.findById(req.session.user, function (err, user) {
            if (err) return next(err);
            if (!user) return next(new HttpError(404, 'User not found'));
            Event.find({user_id: req.session.user}, function (err, events) {
                console.log(events[12]);

                events.forEach(function(item, i, arr) {
                    item.events.sort(compareNumeric);
                });
                console.log(events[12]);

                res.json({
                user: {
                    id: user._id,
                    email: user.email,
                    firstName: user.firstName,
                    lastName: user.lastName,
                },
                events: events
            });
            });

        });

    }


};
