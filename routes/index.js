var express = require('express');
var HttpError = require('../error/index').HttpError;
var ObjectID = require('mongodb').ObjectID;
var path = require('path');
var router = express.Router();

var checkAuth = require('../middleware/checkAuth');
var login = require('./loginUser');
var logout = require('./logout');
var signUp = require('./signUp');
var init = require('./init');
var event = require('./event');

/* GET users listing. */
var User = require('../models/user').User;


router.get('/init', init);
router.post('/login', login.loginPost);
router.post('/logout', logout);
router.post('/signup', signUp.post);
router.post('/add-event', event.post);
router.put('/events', event.put);
router.get('/events', event.get);
router.get('*', function (req, res, next) {
    if (res.req.headers['x-requested-with'] != 'XMLHttpRequest') {
        res.sendFile(path.join(__dirname, "../client/index.html"));
    }
});

module.exports = router;
