let webpack = require('webpack');

module.exports = {
    entry: './client/main.js',
    output: {
        path: __dirname + "/client/build",
        publicPath: "build/",
        filename: "bundle.js"
    },
    devServer: {
        contentBase: __dirname + "/client",
        historyApiFallback: true
    },

    module: {
        rules: [
            {
                test: / \. js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                },

            },
            {
                test: /\.less$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "less-loader" // compiles Less to CSS
                }]

            },
            {
                test: /\.css$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "less-loader" // compiles Less to CSS
                }]

            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader?classPrefix'
            },
            {
                test: /\.(eot|woff|ttf|svg|png|jpg)$/,
                loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
            },
        ]
    }
};

